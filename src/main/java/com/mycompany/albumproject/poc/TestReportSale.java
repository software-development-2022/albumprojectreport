/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.poc;

import com.mycompany.albumproject.model.ReportSale;
import com.mycompany.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author Pinkz7_
 */
public class TestReportSale {
    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//        List<ReportSale> reportDay = reportService.getReportSaleByDay();
//        for(ReportSale r:reportDay){
//            System.out.println(r);
//        }
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for(ReportSale r:reportMonth){
            System.out.println(r);
        }
//        List<ReportSale> reportYear = reportService.getReportSaleByYear();
//        for(ReportSale r:reportYear){
//            System.out.println(r);
//        }
    }
}
